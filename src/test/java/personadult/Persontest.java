package personadult;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class Persontest {
    @Test
    public void test1(){
        Person person = new Person();

        Assertions.assertTrue(person.isAdult(20));

    }
    @Test
    public void test2(){
        Person person = new Person();

        Assertions.assertThrows(IllegalStateException.class, ()-> {
            person.isAdult2();
//            throw new IllegalStateException(message);
        });
    }


}
